import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

public class PunterWinRatiioTest {

    Punter myPunter;
    List<Die> dice;
    Die guess;
    double wins;
    double losses;
    double ratio;

    @Before
    public void setUp() throws Exception {
        myPunter = new Punter("Brad", 10000, 0);
        guess = new Die();
        dice = setUpDice(guess);
        wins = 0;
        losses = 0;
    }

    // Runs a batch game to determine the actual win ratio
    @Test
    public void testWinRatio() {
        play(myPunter, dice);
        //System.out.println("Wins: " + wins);
        //System.out.println("Losses: " + losses);
        ratio = wins / (wins + losses);
        System.out.println("Ratio: " + ratio);
        assertEquals(0.42, ratio, 0.01);
    }

    // Set up dice with same roll every game
    private static List<Die> setUpDice(Die guess) {
        Die d1 = new Die();
        Die d2 = new Die();
        Die d3 = new Die();

        List<Die> dice = new ArrayList<>(Arrays.asList(d1, d2, d3));
        return dice;
    }

    //Replicated InteractiveGame to work without user input, but with automated inputs
    // Guess is always wrong, to simulate an incorrect response
    public void play(Punter punter, List<Die> dice) {
        System.out.println("\nplayBatchMode");

        int initialBalance = punter.getBalance();

        String ans = null;
        System.out.print("\nHow many games? (default 1000): ");
        int numberOfGames = 10000;

        int stdBet = 10;
        System.out.print(String.format("\nEnter standard bet (default %d): ",stdBet));
        ans = "1";
        try {
            stdBet = Integer.parseInt(ans);
        }
        catch (NumberFormatException e) {
            System.out.println("Invalid entry, using default.");
        }

        boolean useStandardPick = false;
        Face pick = null;
        System.out.print(String.format("\nUse standard pick or random (S/R): "));
        ans = "R";

        System.out.println(String.format("\nStarting batch mode game for %s with initial balance %d, limit $%d, and standard bet %d.00",
                punter.getName(), initialBalance, punter.getLimit(), stdBet));

        int roundCount = 0;

        while (roundCount != numberOfGames && punter.balanceExceedsLimitBy(stdBet)) {
            if (!useStandardPick) {
                pick = Face.getRandom();
            }
            System.out.println(String.format("\n%s bets %d on %s, starting with balance $%d",
                    punter.getName(), stdBet, pick, punter.getBalance()));

            int winnings = Round.play(punter, dice, pick, stdBet);
            roundCount++;

            System.out.println(String.format("\nRolled %s, %s, %s",
                    dice.get(0).getFace(), dice.get(1).getFace(), dice.get(2).getFace()));

            if (winnings > 0) {
                System.out.println(String.format("\n%s won %d, balance now %d\n\n",
                        punter.getName(), winnings, punter.getBalance()));
                wins = wins + 1;
            }
            else {
//                System.out.println(String.format("\n%s lost %d, balance now %d\n\n",
//                        punter.getName(), stdBet, punter.getBalance()));
                losses = losses + 1;
            }
        }
        System.out.println(String.format("Player leaves game with $%d after %d rounds, having started with $%d",
                punter.getBalance(), roundCount, initialBalance));

    }
}