# PPPasg4

Professional Programming Practice Assignment 4

Repository Link:
https://bitbucket.org/bradchick/pppasg4/src/master/

Test Directory:
Bug 1 -
Project > src > PunterTest

Bug 2 - 
Project > src > PunterWinTest

Bug 3 -
Project > src > PunterLimitTest

Bug 4 -
Project > src > PunterWinRatiioTest


User Acceptance Test Document aquired from www.vic.gov.au:
https://webcache.googleusercontent.com/search?q=cache:gyfwP0m_tswJ:https://www.vic.gov.au/sites/default/files/2018-09/UAT%2520Template.xlsx+&cd=19&hl=en&ct=clnk&gl=au


Bug Report

1.	Player loses double their bet when they lose.
When a player doesn�t get a match, their balance goes down by twice the amount they bet.

2.	Player doesn�t receive any winnings.
Even when a player gets a match, their balance just stays the same as before they bet.

3.	Player cannot reach betting limit
Even when a placing bet would only take a player to their betting limit, the bet cannot be placed.

4.	Odds in the game are incorrect.
The game should have an approximate win : (win + lose) ratio very close to 0.42 (error < 0.01 over 10000 games)

