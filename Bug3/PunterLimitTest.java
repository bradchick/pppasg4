import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

public class PunterLimitTest {



    Punter myPunter;
    List<Die> dice;
    Die guess;

    @Before
    public void setUp() throws Exception {
        myPunter = new Punter("Brad", 10, 5);
        guess = new Die();
        dice = setUpDice(guess);
    }

    // Test base balance is as indicated
    @Test
    public void testBaseBalanceSame() {
        assertSame(10, myPunter.getBalance());
    }

    // Test base limit is as indicated
    @Test
    public void testBaseLimitSame() {
        assertSame(5, myPunter.getLimit());
    }

    // Runs a game with constant variables, and tests the limits of a bet
    @Test
    public void testLimit() {
        play(myPunter, dice, guess);
        assertSame(5, myPunter.getBalance());
    }

    // Set up dice with same roll every game
    private static List<Die> setUpDice(Die guess) {
        Die d1;

        do {
            d1 = new Die();
        } while (d1.getFace() == guess.getFace());

        List<Die> dice = new ArrayList<>(Arrays.asList(d1, d1, d1));
        return dice;
    }

    //Replicated InteractiveGame to work without user input, but with automated inputs
    // Guess is always wrong, to simulate an incorrect response
    public void play(Punter punter, List<Die> dice, Die guess) {
        System.out.println("\nplayInteractive");

        int initialBalance = punter.getBalance();

        int stdBet = 5;
        System.out.print(String.format("\nEnter standard bet (default %d): ",stdBet));
        String ans = "5";
        try {
            stdBet = Integer.parseInt(ans);
        }
        catch (NumberFormatException e) {
            System.out.println("Invalid entry, using default.");
        }

        System.out.println(String.format("\nStarting interactive game for %s with initial balance $%d.00, limit $%d.00, and standard bet %d.00",
                punter.getName(), initialBalance, punter.getLimit(), stdBet));
        String betPrompt = "Select Symbol: 1 - Fish, 2 - Prawn, 3 - Crab, 4 - Rooster, 5 - Gourd, 6 - Stag";

        Random random = new Random();
        int roundCount = 0;
        boolean stop = false;
        while (!stop) {
            int selection;
            Face pick = null;

            System.out.println(betPrompt);

            // Select guess that will not be picked
            switch(guess.getFace().toString()) {
                case "Fish":
                    ans = "1";
                    break;
                case "Prawn":
                    ans = "2";
                    break;
                case "Crab":
                    ans = "3";
                    break;
                case "Rooster":
                    ans = "4";
                    break;
                case "Gourd":
                    ans = "5";
                    break;
                case "Stag":
                    ans = "6";
                    break;
            }

            try {
                selection = Integer.parseInt(ans);
                if (selection < 1 || selection > 6) {
                    throw new NumberFormatException();
                }
                pick = Face.getByIndex(selection-1);
                System.out.println(String.format("Selected %s.", pick));
            }
            catch (NumberFormatException e) {
                selection = random.nextInt(6);
                pick = Face.getByIndex(selection);
                System.out.println(String.format("Invalid entry, using %s.", pick));
            }

            System.out.print(String.format("\nEnter  bet (default $%d): ", stdBet));
            ans = "5";
            int bet = stdBet;
            try {
                bet = Integer.parseInt(ans);
            }
            catch (NumberFormatException e) {
                System.out.println("Invalid entry, using default.");
            }

            if (!punter.balanceExceedsLimitBy(bet)) {
                System.out.println(String.format("Betting %d could go below limit, voiding bet", bet));
                break;
            }

            System.out.println(String.format("\n%s bets %d on %s, starting with balance $%d",
                    punter.getName(), bet, pick, punter.getBalance()));

            int winnings = Round.play(punter, dice, pick, bet);
            roundCount++;

            System.out.println(String.format("\nRolled %s, %s, %s",
                    dice.get(0).getFace(), dice.get(1).getFace(), dice.get(2).getFace()));

            if (winnings > 0) {
                System.out.println(String.format("\n%s won %d, balance now %d\n\n",
                        punter.getName(), winnings, punter.getBalance()));
            }
            else {
                System.out.println(String.format("\n%s lost %d, balance now %d\n\n",
                        punter.getName(), bet, punter.getBalance()));
            }

            System.out.println("\nPlay again (Y/N)? (default: Y) ");
            ans = "N";
            if (ans.equalsIgnoreCase("N")) {
                stop = true;
            }

        }
        System.out.println(String.format("Player leaves game with $%d after %d rounds, having started with $%d",
                punter.getBalance(), roundCount, initialBalance));

    }
}